# Infra as Code tools

Click the button below to start a new GitPod environment:
[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/thegaragebandofit/infra-as-code-tools/)

A `Docker` image that is able to use `gcloud`, `terraform`, `packer`, etc.  
This `Docker`image is to be used by _CI/CD pipeline_ tools.
