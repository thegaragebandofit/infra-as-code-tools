FROM google/cloud-sdk:alpine
MAINTAINER "Ludovic Piot <ludovic.piot@soat.fr>"

# OpenSSL
RUN apk add openssl

# Git
RUN apk add git

# GCP vars
ENV GOOGLE_SERVICE_ACCOUNT=myproject-terraform-accnt@lof-ws-test.iam.gserviceaccount.com
ENV GOOGLE_PROJECT=myproject

# GCP configuration

WORKDIR /
RUN gcloud config set account ${GOOGLE_SERVICE_ACCOUNT}
RUN gcloud config set project ${GOOGLE_PROJECT}

# Terraform vars
ENV TF_VERSION=0.11.13

# Terraform install
WORKDIR /usr/bin
RUN wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip && \
    unzip ./terraform_${TF_VERSION}_linux_amd64.zip

# Packer vars
ENV PKR_VERSION=1.4.0

# Packer install
WORKDIR /usr/bin
RUN wget https://releases.hashicorp.com/packer/${PKR_VERSION}/packer_${PKR_VERSION}_linux_amd64.zip && \
    unzip ./packer_${PKR_VERSION}_linux_amd64.zip

ENTRYPOINT [ "/bin/sh" ]
